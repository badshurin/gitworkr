package ru.badshurin;

public class Hello {

    public String name;

    public Hello(){}

    public Hello(String name) {
        this.name = name;
    }

    public String hello() {
        return "Hello " + name + "!";
    }

    public static void main(String[] args) {
        System.out.println(new Hello("Alex").hello());
    }
}
